﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core;
using FlaUI.UIA3;
using FlaUI.UIA2;
using FlaUI.Core.Conditions;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Tools;
using FlaUI.Core.WindowsAPI;
using FlaUI.Core.Input;
using System.Threading;

namespace FlaUI_Example01
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = FlaUI.Core.Application.Launch("notepad.exe");
          
            using (var automation = new UIA3Automation())
            {
                var window = app.GetMainWindow(automation);
              
                Console.WriteLine("App title:" +window.Title);

                ConditionFactory fr = new ConditionFactory(new UIA3PropertyLibrary());

                //find the document using automation ID
                var doc = window.FindFirstDescendant(x => x.ByAutomationId("15"));
                    doc.AsTextBox().Text = "i want to print some text...";
                Console.WriteLine("Adding some text");
                var fileMenu = window.FindFirstDescendant(x => x.ByName("File"));
                    Thread.Sleep(1000);
                    fileMenu.DrawHighlight();
                    fileMenu.Click();
             
                var printOpt = window.FindFirstDescendant(x => x.ByName("Print..."));
                var com=Task.Run(()=>printOpt.Click());
                while (com.IsCompleted == false)
                {
                    Console.WriteLine("Waiting for print dialog to be present...");
                    Thread.Sleep(1000);
                }
                var printBtn = window.FindFirstDescendant(x => x.ByAutomationId("1").And(x.ByText("Print")));
                printBtn.DrawHighlight();
                printBtn.AsButton().Click();
                Console.WriteLine("Send to printer");

            }
        }
    }
}
